// получаем єлементи с DOM
const themeBtn = document.getElementById("theme-button");
const colorSpan = document.getElementById("color-span");
const colorH2 = document.getElementById("color-h2");
const colorP = document.getElementById("color-p");
const secondH2 = document.getElementById("second-h2");

// делаем функцыю которая будет менят цвет
function toggleColor() {
  const body = document.body;
  const firstTheme = body.className;

  // делаем проверку для цвета и меняем его
  if (firstTheme === "dark") {
    body.className = "light";
    localStorage.setItem("theme", "light");

    colorSpan.style.color = "#333333";
    colorH2.style.color = "#333333";
    colorP.style.color = "#333333";
    secondH2.style.color = "#333333";
  } else {
    body.className = "dark";
    localStorage.setItem("theme", "dark");

    colorSpan.style.color = "#ffffff";
    colorH2.style.color = "#ffffff";
    colorP.style.color = "#ffffff";
    secondH2.style.color = "#ffffff";
  }
}

//добавляем клик на кнопку
themeBtn.addEventListener("click", toggleColor);

// проверям сохранена ли тема и переключаем её
const savedTheme = localStorage.getItem("theme");

if (savedTheme) {
  document.body.className = savedTheme;

  // ьеняем цвет элементов под тему
  if (savedTheme === "light") {
    colorSpan.style.color = "#333333";
    colorH2.style.color = "#333333";
    colorP.style.color = "#333333";
    secondH2.style.color = "#333333";
  } else {
    colorSpan.style.color = "#ffffff";
    colorH2.style.color = "#ffffff";
    colorP.style.color = "#ffffff";
    secondH2.style.color = "#ffffff";
  }
}
